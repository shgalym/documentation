# Адреса приложений
---
## Сервис авторизации (KYC)

### develop
Кабинет пользователя [https://kyc.getcrypto24test.com](https://kyc.getcrypto24test.com)  
Бэкенд [https://auth8.azurewebsites.net](https://auth8.azurewebsites.net)

### master
Кабинет пользователя [https://kyc.getcrypto24.com](https://kyc.getcrypto24.com)  
Бэкенд [https://crnc-exchng-auth.azurewebsites.net/](https://crnc-exchng-auth.azurewebsites.net/)

---
## Обменник (GetCrypto24)

### develop
Кабинет пользователя [https://getcrypto24test.com](https://getcrypto24test.com), доступен так же как https://www.getcrypto24test.com  
Кабинет администратора [https://admin.getcrypto24test.com](https://admin.getcrypto24test.com)  
Бэкенд [https://getcrypto24test.com/api/](https://getcrypto24test.com/api/)  

### master
Кабинет пользователя [https://getcrypto24.com](https://getcrypto24.com), доступен так же как https://www.getcrypto24.com, https://bitcoinalley.com, https://www.bitcoinalley.com  
Кабинет администратора [https://admin.getcrypto24.com](https://admin.getcrypto24.com), доступен так же как https://admin.bitcoinalley.com  
Бэкенд [https://getcrypto24.com/api/](https://getcrypto24.com/api/)  

---
## Процессинг

### develop
Бэкенд [https://prc.getcrypto24test.com](https://prc.getcrypto24test.com)

### master
Бэкенд [https://prc.getcrypto24.com](https://prc.getcrypto24.com)

---
## GoToCrowd
Фронтенд [https://gotocrowd.azurewebsites.net](https://gotocrowd.azurewebsites.net)

## WeedPay

### master
Фронтенд [https://weedpay.org](https://weedpay.org)

Фронтенд управления учетными записями торговцев [https://account.weedpay.org](https://account.weedpay.org)

Платежный виджет [https://gateway.weedpay.org](https://gateway.weedpay.org)

### develop
Фронтенд [https://weedpay.dev](https://weedpay.dev)

Фронтенд управления учетными записями торговцев [https://account.weedpay.dev](https://account.weedpay.dev)


## GoToCrypto

### master
Фронтенд [https://gotocrypto.cash](https://gotocrypto.cash)

Фронтенд управления учетными записями торговцев [https://account.gotocrypto.cash](https://account.gotocrypto.cash)

Платежный виджет [https://go2crypto.express](https://go2crypto.express)

Фронтенд SSO [https://login.gotocrypto.cash](https://login.gotocrypto.cash)


### develop
Фронтенд [https://go2crypto.dev](https://go2crypto.dev)

Фронтенд управления учетными записями торговцев [https://account.go2crypto.dev](https://account.go2crypto.dev)

Фронтенд SSO [https://login.go2crypto.dev](https://login.go2crypto.dev)


## RabbitMQ брокер сообщений

### develop
Hostname: **rabbid**

User: **poll**

Password: **Un@nslZ4#Pa$$e!**
