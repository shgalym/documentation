# Требования к учету операций в платежной системе (черновик)

## Order of operations

1. Принять информацию о `заказе` покупателя и сохранить заказ, включая информацию о реализуемых товарах или услугах, сопоставив его в системе с записью `покупателя`, и записью `продавца`.
2. Привязать к принятому заказу `одноразовый платежный адрес` для оплаты и передать его `покупателю`.
3. Получить информацию о `поступлении оплаты` на `одноразовый платежный адрес`
4. Сопоставить информацию о `поступлении оплаты` на `одноразовый платежный адрес` с заказом покупателя.
5. В случае, если средств на `платежном адресе` достаточно для полной оплаты `заказа`, создать `заявку` на вывод средств на `транзитный счет` продавца в системе обмена валют.
6. Уведомить систему обмена валют об отправке средств на `транзитный счет` продавца.
7. Получить информацию об `успешной отправке средств` на `транзитный счет` продавца.
8. Создать заявку на обмен crypto средств `транзитном счете` продавца на средства в ФИАТ-ной валюте.
9. Получить подтверждение системы обмена валют об успешном обмене crypto средств на ФИАТ и отправке их на банковский счет продавца.
10. Уведомить продавца об успешной оплате покупателем счета

## Step by step details

### How to process merchant order?

1. Проверяем, не существует ли полученный заказ уже в системе? Если существует, то возвращаем ранее созданный в системе счет на оплату в crypto валюте с привязанным временным платежным адресом.
2. Если заказ еще не существует, то сохраняем в системе полную информацию о заказе и возвращаем покупателю счет на оплату в crypto валюте с привязанным временным платежным адресом.

В системе сохраняем полную информацию о заказе `MerchantOrder`:

```json
{
  "MerchantId": "2938",
  "MerchantName": "ACME devices",
  "MerchantTaxId": "092-02394-39917",
  "MerchantAddress": "US, California, San Diego, 12923, 3948 Silent drive",
  "MerchantPhone": "1 80093827394",
  "CustomerName": "Joe Doe",
  "CustomerTaxId": "398-93-2038",
  "CustomerAddress": "US, District Columbia, Washington, 90291, 9388 Guard street",
  "CustomerPhone": "1 3892918384",
  "OrderNumber": "AD-38234",
  "OrderDate": "2019-10-10T11:36:24",
  "OrderDescription": "Mobile phone spare parts",
  "OrderCurrency": "USD",
  "Amount": "39.95",
  "VAT": "6.09",
  "Id": "233",
  "CreatedDateTime": "2019-11-10T19:08:49",
  "CreatorAddress": "5.155.29.88",
  "CreatorUserName": "Processing Service",
  "CreatorUserId": "3827"
}
```

Информацию о строках заказа `MerchantOrderItem`:
```json
[
  {
    "MerchantOrderId": "233",
    "LineNumber": "1",
    "SKU": "3898237",
    "Name": "Samsung touch screen",
    "ItemPrice": "8.75",
    "Quantity": "2",
    "TotalPrice": "17.50"
  },
  {
    "MerchantOrderId": "233",
    "LineNumber": "2",
    "SKU": "DKLSN-298",
    "Name": "Battery mount screw",
    "ItemPrice": "0.11",
    "Quantity": "30",
    "TotalPrice": "3.30"
  }
]
```

Информацию о счете для покупателя `CustomerInvoice`:
```json
  {
    "MerchantOrderId": "233",
    "OrderNumber": "AD-38234",
    "OrderDate": "2019-10-10T11:36:24",
    "OrderDescription": "Mobile phone spare parts",
    "OrderCurrency": "USD",
    "OrderAmount": "39.95",
    "CustomerName": "Joe Doe",
    "CustomerTaxId": "398-93-2038",
    "CustomerAddress": "US, District Columbia, Washington, 90291, 9388 Guard street",
    "CustomerPhone": "1 3892918384",
    "InvoiceNumber": "3982",
    "InvoiceDate": "2019-11-10T11:36:28",
    "InvoiceCurrency": "ETH",
    "ExchangeRate": "0.0047",
    "ExchangeProvider": "https://floating.group",
    "InvoiceAmount": "0.187765",
    "Commission": "0.00011",
    "InvoiceTotal": "0.187875",
    "PayeeAddress": "0x203940981230981209830192",
    "Id": "387",
    "DateTime": "2019-11-10T11:40:51",
    "CreatorAddress": "5.155.29.88",
    "CreatorUserName": "Processing Service",
    "CreatorUserId": "3827"
  }
```

### How to process customer crypto payments?

При получении информации о поступлении средств на временный платежный адрес заказа, информация о каждой новой транзакции сохраняется в платежной системе `CustomerCryptoPayment`:

```json
{
  "CustomerInvoiceId": "378",
  "CustomerInvoiceNumber": "3982",
  "MerchantOrderId": "233",      
  "OrderNumber": "AD-38234",
  "Timestamp": "2019-15-06T09:26:10",
  "Transaction": "0x3c2f2995228c1c29ef121cd1db85feb39f9d50e9660a751b3bf509243e1797c2",
  "PayerAddress": "0xefca89f5d2ad9fb8f50710b835396b197beca354",
  "PayeeAddress": "0x1b75b90e60070d37cfa9d87affd124bb345bf70a",
  "Currency": "ETH",
  "Amount": "0.005909",
  "Id": "199",
  "DateTime": "2019-11-10T11:40:51",
  "CreatorAddress": "5.155.29.88",
  "CreatorUserName": "Processing Service",
  "CreatorUserId": "3827"
}
```

При обработке полученных транзакций, счета которые были полностью оплачены, в системе сохраняется запись `PaidCustomerInvoice`:

```json
{
  "CustomerInvoiceId": "378",
  "CustomerInvoiceNumber": "3982",
  "MerchantOrderId": "233",      
  "MerchantOrderNumber": "AD-38234",
  "MerchantOrderCurrency": "USD",
  "MerchantOrderAmount": "39.95",
  "CustomerInvoiceCurrency": "ETH",
  "CustomerInvoiceAmount": "0.187765",
  "ExchangeProvider": "https://floating.group",
  "ExchangeProviderRate": "0.0047",
  "ExchangeProviderCommission": "0.00011",
  "InvoiceTotal": "0.187875",
  "PayeeAddress": "0x1b75b90e60070d37cfa9d87affd124bb345bf70a",
  "Id": "239",
  "DateTime": "2019-11-10T11:40:51",
  "CreatorAddress": "5.155.29.88",
  "CreatorUserName": "Processing Service",
  "CreatorUserId": "3827"
}
```

Таким образом мы делаем экземпляры собственно `CustomerInvoice` неизменяемыми. Никаких статусов, которые нужно обновлять. Созданная однажды запись, больше не редактируется.

Для масштабирования обработки заказов можем к примеру после сохранения экземпляра `PaidCustomerInvoice` в базе, копию его отправлять во временное хранилище на базе `Redis`. 

После этого, отдельный агент, сможет забрать копию `PaidCustomerInvoice` из хранилища `Redis`, выполнить необходимую процедуру обработки и удалить экземпляр `PaidCustomerInvoice` из хранилища `Redis`. 

### How to purchase FIAT?

Процедура закупки ФИАТ-а инициируется появлением во временном `Redis` хранилище записи `PaidCustomerInvoice`.

Агент, обрабатывающий это уведомление создает `заявку на вывод средств` на `транзитный счет` продавца в системе обмена валют `CryptoTransferOrder`:

```json
{
  "CustomerInvoiceId": "378",
  "CustomerInvoiceNumber": "3982",
  "MerchantOrderId": "233",      
  "MerchantOrderNumber": "AD-38234",
  "MerchantOrderCurrency": "USD",
  "MerchantOrderAmount": "39.95",
  "CustomerInvoiceCurrency": "ETH",
  "CustomerInvoiceAmount": "0.187765",
  "ExchangeProvider": "https://floating.group",
  "ExchangeProviderRate": "0.0047",
  "ExchangeProviderCommission": "0.00011",
  "InvoiceTotal": "0.187875",
  "PayerAddress": "0x1b75b90e60070d37cfa9d87affd124bb345bf70a",
  "PayeeAddress": "0xefca89f5d2ad9fb8f50710b835396b197beca354",
  "Id": "472",
  "DateTime": "2019-11-10T11:40:51",
  "CreatorAddress": "5.155.29.88",
  "CreatorUserName": "Processing Service",
  "CreatorUserId": "3827"  
}
```


## End points

 1. [Заказы продавцов](#markdown-header-merchant-orders) `/api/merchant-order`
 2. [Счета платежной системы покупателям](#markdown-header-customer-invoices) `/api/customer-invoice`
 3. [Платежи покупателей](#markdown-header-customer-payments) `/api/customer-payment`
 4. [Покупка ФИАТ](#markdown-header-)

## Merchant orders

Информация доступна пользователям, имеющим роль `View merchant order`.

`URL` точки подключения `/api/merchant-order`.

`GET` запрос должен возвращать информацию о переданных в оплату заказах продавцов в виде списка.

```json
  [
    {
      "MerchantId": "2938",
      "MerchantName": "ACME devices",
      "MerchantTaxId": "092-02394-39917",
      "MerchantAddress": "US, California, San Diego, 12923, 3948 Silent drive",
      "MerchantPhone": "1 80093827394",
      "CustomerName": "Joe Doe",
      "CustomerTaxId": "398-93-2038",
      "CustomerAddress": "US, District Columbia, Washington, 90291, 9388 Guard street",
      "CustomerPhone": "1 3892918384",
      "OrderNumber": "AD-38234",
      "OrderDate": "2019-10-10T11:36:24",
      "OrderDescription": "Mobile phone spare parts",
      "OrderCurrency": "USD",
      "Amount": "39.95",
      "VAT": "6.09",
      "Id": "233",
      "DateTime": "2019-11-10T19:08:49",
      "CreatorAddress": "5.155.29.88",
      "CreatorUserName": "Processing Service",
      "CreatorUserId": "3827"
    }
  ]
```

### Приемочные тесты

1. Автоматически генерируем набор заказов продавца и отправляем их бэкенду процессинга
2. Отправляем запрос на `/api/merchant-order` и сверяем полученные данные с набором сгенерированных в предыдущем пункте заказов

### Макет представления списка заказов (черновик)
![picture](Billing/merchant-orders.jpg)

## Customer invoices

Информация доступна пользователям, имеющим роль `View customer invoice`.

`URL` точки подключения `/api/customer-invoice`.

`GET` запрос должен возвращать информацию о полученных покупателями счетах в виде списка.

```json
  [
    {
      "MerchantOrderId": "233",
      "OrderNumber": "AD-38234",
      "OrderDate": "2019-10-10T11:36:24",
      "OrderDescription": "Mobile phone spare parts",
      "OrderCurrency": "USD",
      "OrderAmount": "39.95",
      "CustomerName": "Joe Doe",
      "CustomerTaxId": "398-93-2038",
      "CustomerAddress": "US, District Columbia, Washington, 90291, 9388 Guard street",
      "CustomerPhone": "1 3892918384",
      "InvoiceNumber": "3982",
      "InvoiceDate": "2019-11-10T11:36:28",
      "InvoiceCurrency": "ETH",
      "ExchangeRate": "0.0047",
      "ExchangeProvider": "https://fpg.com",
      "InvoiceAmount": "0.187765",
      "Commission": "0.00011",
      "InvoiceTotal": "0.187875",
      "PayeeAccountNumber": "0x203940981230981209830192",
      "Id": "387",
      "DateTime": "2019-11-10T11:40:51",
      "CreatorAddress": "5.155.29.88",
      "CreatorUserName": "Processing Service",
      "CreatorUserId": "3827"
    }
  ]
```

### Приемочные тесты

1. Автоматически генерируем набор заказов продавца и отправляем их бэкенду процессинга
2. Отправляем запрос на `/api/merchant-order` и сверяем полученные данные с набором сгенерированных в предыдущем пункте заказов
3. Отправляем запрос на `/api/customer-invoice` и проверяем что для каждого заказа, полученного в предыдущем пункте существует соответствующий счет

### Макет представления списка счетов (черновик)
![picture](Billing/invoices.png)

## Customer payments

Информация доступна пользователям, имеющим роль `View customer payment`.

`URL` точки подключения `/api/customer-payment`.

`GET` запрос должен возвращать информацию о выполненных покупателями платежах в виде списка.

```json
  [
    {
      "InvoiceId": "378",
      "InvoiceNumber": "3982",
      "MerchantOrderId": "233",      
      "OrderNumber": "AD-38234",
      "Timestamp": "2019-15-06T09:26:10",
      "Transaction": "0x3c2f2995228c1c29ef121cd1db85feb39f9d50e9660a751b3bf509243e1797c2",
      "PayerAddress": "0xefca89f5d2ad9fb8f50710b835396b197beca354",
      "PayeeAddress": "0x1b75b90e60070d37cfa9d87affd124bb345bf70a",
      "Currency": "ETH",
      "Amount": "0.005909",
      "Id": "199",
      "DateTime": "2019-11-10T11:40:51",
      "CreatorAddress": "5.155.29.88",
      "CreatorUserName": "Processing Service",
      "CreatorUserId": "3827"
    }
  ]
```

### Макет представления списка платежей (черновик)
![picture](Billing/payments.png)
