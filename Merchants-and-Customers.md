# Merchants and customers API

## Staging application base URL

`https://accounts.go2crypto.dev/api`

## Required headers

Application expects all requests must supply valid HTTP headers:

 - Origin
 - Referer
 - Accept-Language
 - User-Agent

## Endpoints

1. User registration [/api/account/register](#markdown-header-user-registration)
2. User login [/api/account/login](#markdown-header-user-login)

## User registration

 - `/api/account/register` endpoint
 - method `POST`
 - content type `application/json`
 - request body example:
```json
{
  "Email": "92837498@mail.local",
  "Phone": "+1 893 833 9482",
  "Password": "P@ssw0rd"
}
```
 - response body example:
```json
{
  "Id":34,
  "Name":"92837498@mail.local",
  "Email":"92837498@mail.local",
  "Phone":"1 8938339482",
  "Password":"is not real password",
  "IsEmailConfirmed":false,
  "IsPhoneVerified":false,
  "IsAccessDenied":false,
  "EditorAddress":"::ffff:10.0.2.43",
  "EditDateTime":"2019-09-03T03:35:52.9553068Z",
  "OwnerUserId":0,
  "OwnerGroupId":0,
  "EditorUserId":0,
  "EditorGroupId":0
}
```

### Command line CURL example requests

`OPTIONS` request example:
```
curl -X "OPTIONS" -H "Content-Type: application/json" -H "Origin: https://account.go2crypto.dev" -H "Referer: https://account.go2crypto.dev" -H "Accept-Language: en-US" -H "User-Agent: windows command line curl" -H "Access-Control-Request-Headers: content-type" -H "Access-Control-Request-Method: POST" -i https://account.go2crypto.dev/api/account/register
```

`POST` request example:
```
curl -X "POST" -H "Content-Type: application/json" -H "Origin: https://account.go2crypto.dev" -H "Referer: https://account.go2crypto.dev" -H "Accept-Language: en-US" -H "User-Agent: windows command line curl" -d "{'Email':'punk@mail.local','Phone':'+7 999 888 70 70','Password':'P@ssw0rd'}" -i https://account.go2crypto.dev/api/account/register
```

## User login

 - `/api/account/login` endpoint
 - method `POST`
 - content type `application/json`
 - request body example:
```json
{
  "Login": "punk@mail.local",
  "Password": "P@ssw0rd"
}
```
 - response body example:
```json
{
  "UserId":34,
  "GroupId":28,
  "Groups":["punk@mail.local"],
  "Roles":[],
  "AccessToken":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9zaWQiOiIzNCIsImVtYWlsIjoicHVua0BtYWlsLmxvY2FsIiwibmFtZWlkIjoicHVua0BtYWlsLmxvY2FsIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbW9iaWxlcGhvbmUiOiI3IDk5OTg4ODcwNzAiLCJncm91cHNpZCI6InB1bmtAbWFpbC5sb2NhbCIsIm5iZiI6MTU2Nzc0OTc2NCwiZXhwIjoxNTY3Nzg1NzY0LCJpYXQiOjE1Njc3NDk3NjQsImlzcyI6Imh0dHBzOi8vYWNjb3VudC5nbzJjcnlwdG8uZGV2IiwiYXVkIjoiaHR0cHM6Ly9hY2NvdW50LmdvMmNyeXB0by5kZXYifQ.dHbdWFcbkzc74OTl2LPIC1oBX595kuvpv_DbStq6jKujVaNa30pEdaVjOigkOnA5C4zowdWv5qmq6cXyEgU0cA",
  "TokenExpirationDate":"2019-09-06T16:02:44.4686522Z",
  "UserName":"punk@mail.local",
  "Email":"punk@mail.local",
  "Phone":"7 9998887070"
}
```

### Command line CURL example requests

`OPTIONS` request example:
```
curl -X "OPTIONS" -H "Content-Type: application/json" -H "Origin: https://account.go2crypto.dev" -H "Referer: https://account.go2crypto.dev" -H "Accept-Language: en-US" -H "User-Agent: windows command line curl" -H "Access-Control-Request-Headers: content-type" -H "Access-Control-Request-Method: POST" -i https://account.go2crypto.dev/api/account/login
```

`POST` request example:
```
curl -X "POST" -H "Content-Type: application/json" -H "Origin: https://account.go2crypto.dev" -H "Referer: https://account.go2crypto.dev" -H "Accept-Language: en-US" -H "User-Agent: windows command line curl" -d "{'Login':'punk@mail.local','Password':'P@ssw0rd'}" -i https://account.go2crypto.dev/api/account/login
```