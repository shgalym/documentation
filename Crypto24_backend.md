# Бекенд обменника

## Select messages language

Для выбора языка сообщений ответов методов API, необходимо добавлять в HTTP заголовки запросов стандартный заголовок. 

        'Accept-Language': 'en'

При переключении локали в расширении браузера он подставляется сам, таким образом ручное его добавление скорее всего не потребуется.

В поле значения передается локаль в двухбуквенном формате.

Если указанный в заголовке язык отсутствует в приложении, ответ будет возвращен на английском языке.

На данный момент других локалей нет, поэтому данный параграф можно игнорировать.

## Get currencies list

- Точка подключения `/api/CurrencyList`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Входных данных не требуется. В случае серверной ошибки вернется ответ с кодом `500` в формате

    {
        "error": "message"
    }

В случае успеха вернется:

    [
    {
        "name":"Bitcoin",
        "code":"BTC"
    },
    {
        "name":"Ethereum",
        "code":"ETH"
    },
    {
        "name":"United States Dollar",
        "code":"USD"
    },
    {
        "name":"Euro",
        "code":"EUR"
    }
    ]

## Get currency price

- Точка подключения `/api/Price`
- Метод `POST`
- Ожидаемый тип данных `application/json`
- Поля модели, принимаемой методом (все обязательные)

    - **SellCurrency** - валюта, которую клиент продает. Принимаются только трехбуквенные коды из метода выше.

    - **SellAmount** - количество валюты, которую клиент продает. Десятичная форма, разделитель - точка `.`

    - **BuyCurrency** - валюта, которую клиент покупает. Принимаются только трехбуквенные коды из метода выше.

    - **BuyAmount** - количество валюты, которую клиент покупает. Десятичная форма, разделитель - точка `.`

    - **Direction** - булевое значение, направление конвертации. 
    ***`True`*** - если известна цена, вычисляем количество покупаемой валюты. 
    ***`False`*** - если известно количество покупаемой валюты, вычисляем цену. 

Ошибки:

- Если передан несуществующий код валюты, возвращается ответ с кодом `400`

- Если не указана сумма или она равна нулю, возвращается ответ с кодом `422` 

Пример запроса:

    {
        "SellCurrency":"ETH",
        "SellAmount":"10",
        "BuyCurrency":"BTC",
        "BuyAmount":10,
        "Direction":false
    }

Пример ответа:

    0.00640823

# Orders
## Create order

- Точка подключения `/api/Order`
- Метод `POST`
- Ожидаемый тип данных `application/json`
- Поля модели, принимаемой методом (все обязательные)

- **Address** - адрес, на который будет отправлена купленная крипта. Должен быть валидным адресом выбранной крипты (**To**)

- **Price** - цена заказа в десятичной форме

- **From** - валюта, в которой клиент платит (BTC|ETH)

- **To** - валюта, которую клиент получает (BTC|ETH)

Ошибки:

- Если обнаружена ошибка во входных данных, вернется ответ с кодом `422` в формате 

    {
        "error": "message"
    }
- В случае верверной ошибки вернется ответ с кодом `500` в формате

    {
        "message": "message"
    }

В случае успеха возвращается ответ с кодом `201`:

Пример ответа 

    {
        "orderId": 3010,
        "userId": "3",
        "userEmail": "Tony Stark",
        "processingOrderId": "52",
        "from": "ETH",
        "to": "BTC",
        "orderStatus": 0,
        "receiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
        "paymentAddress": "0x2216df99909fFb3292bBB0BD0CAd8CaEDC019435",
        "amount": 2818366,
        "decAmount": 0.02818366,
        "strAmount": "0.02818366",
        "price": 1000000000000000000,
        "decPrice": 1,
        "strPrice": "1"
    }
 
Где:
- **orderId** - идентификатор заказа в обменнике
- **userId** - идентификатор пользователя
- **userEmail** - мейл пользователя (пока подставляется юзернейм)
- **processingOrderId** - идентификатор заказа в процессинге
- **from** - валюта, в которой платит клиент
- **to** - валюта, которую клиент покупает
- **orderStatus** - статус заказа (0 - ожидает оплаты)
- **receiverAddress** - адрес получателя
- **paymentAddress** - платежный адрес
- **amount** - количество приобретаемой крипты (сумма заказа) в сатошах/веях
- **decAmount** - сумма заказа в биткоинах/эфирах
- **strAmount** - значение выше в формате строки
- **price** - цена заказа в сатошах/веях
- **decPrice** - цена заказа в биткоинах/эфирах
- **strPrice** - цена в формате строки


## Get user's orders

- Точка подключения `/api/Orders`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Параметры:

- **PageNumber** - номер запрашиваемой страницы
- **ItemsPerPage** - количество элементов на странице

Возвращает заказы пользователя, который указан в токене

Пример ответа:

    {
        "TotalItems": 5,
        "Items": [
            {
                "Id": 1,
                "Created": "2018-12-07T23:53:25.3688274",
                "ProcessingOrderId": "131",
                "From": "ETH",
                "To": "BTC",
                "OrderStatus": 0,
                "PaymentAddress": "0x566933EF6D103E8c998543625DD5722Dd5fD7C33",
                "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
                "Amount": "0.07429001",
                "Price": "3"
            },
            {
                "Id": 2,
                "Created": "2018-12-07T23:53:32.3069648",
                "ProcessingOrderId": "132",
                "From": "ETH",
                "To": "BTC",
                "OrderStatus": 0,
                "PaymentAddress": "0x570b5dfc4d6555c6e5237f16a30b7dEa0544E379",
                "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
                "Amount": "0.0742901",
                "Price": "3"
            }
        ]
    }

## Get full orders

- Точка подключения `/api/FullOrders`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Параметры:

- **PageNumber** - номер запрашиваемой страницы
- **ItemsPerPage** - количество элементов на странице

Возвращает заказы всех пользователей со всей информацией. Токен должен принадлежать админу

Пример ответа:

    {
        "TotalItems": 2,
        "Items": [
            {
                "Email": "Tony Stark",
                "UserId": "3",
                "Id": 1,
                "Created": "2018-12-07T23:53:25.3688274",
                "ProcessingOrderId": "131",
                "From": "ETH",
                "To": "BTC",
                "OrderStatus": 0,
                "PaymentAddress": "0x566933EF6D103E8c998543625DD5722Dd5fD7C33",
                "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
                "Amount": "0.07429001",
                "Price": "3"
            },
            {
                "Email": "Tony Stark",
                "UserId": "3",
                "Id": 2,
                "Created": "2018-12-07T23:53:32.3069648",
                "ProcessingOrderId": "132",
                "From": "ETH",
                "To": "BTC",
                "OrderStatus": 0,
                "PaymentAddress": "0x570b5dfc4d6555c6e5237f16a30b7dEa0544E379",
                "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
                "Amount": "0.0742901",
                "Price": "3"
            }
        ]
    }

## Get order

Получение одного заказа пользователя

- Точка подключения `/api/Order/{id}`
- Метод `GET`
- Ожидаемый тип данных `application/json`

        {
            "Id": 1,
            "Created": "2018-12-07T23:53:25.3688274",
            "ProcessingOrderId": "131",
            "From": "ETH",
            "To": "BTC",
            "OrderStatus": 0,
            "PaymentAddress": "0x566933EF6D103E8c998543625DD5722Dd5fD7C33",
            "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
            "Amount": "0.07429001",
            "Price": "3"
        }

## Get full order

Получение полной информации по заказу (для админки)

- Точка подключения `/api/FullOrders/{id}`
- Метод `GET`
- Ожидаемый тип данных `application/json`

        {
            "OrderId": 1,
            "Created": "2018-12-07T23:53:25.3688274",
            "UserId": "3",
            "UserEmail": "Tony Stark",
            "ProcessingOrderId": "131",
            "From": "ETH",
            "To": "BTC",
            "OrderStatus": 0,
            "ReceiverAddress": "12dR1KHJtezgQnK2ArPxyh9qjZqtF5YRYr",
            "PaymentAddress": "0x566933EF6D103E8c998543625DD5722Dd5fD7C33",
            "Amount": 7429001,
            "DecAmount": 0.07429001,
            "StrAmount": "0.07429001",
            "Price": 3000000000000000000,
            "DecPrice": 3,
            "StrPrice": "3",
            "Fee": 303979,
            "DecFee": 0.00303979,
            "StrFee": "0.00303979"
        }


# Fees
## Get fees list

- Точка подключения `/api/Fees`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Пример ответа:

    {
    "TotalItems": 2,
    "Items": [
        {
        "Id": 1,
        "Percent": 3,
        "Absolute": 44,
        "MaxAmount": 1000
        },
        {
        "Id": 2,
        "Percent": 3,
        "Absolute": 44,
        "MaxAmount": 2222
        }
    ]
    }

## Add fee

- Точка подключения `/api/Fees`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
        "Percent": 3,
        "Absolute": 44,
        "MaxAmount": 1000
    }

Где:

- **Percent** - процентная величина комиссии
- **Absolute** - абсолютная величина комиссии
- **MaxAmount** - верхняя граница применения этой комиссии

Пример ответа:

    {
        "id": 3,
        "percent": 3,
        "absolute": 44,
        "maxAmount": 1000
    }

## Edit fee

- Точка подключения `/api/Fees/{id}`
- Метод `PUT`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
        "Id": 2,
        "Percent": 1,
        "Absolute": 8,
        "MaxAmount": 1
    }

*Id в URL'е и в модели должны совпадать*

В ответ приходит код `204` без тела

## Delete fee

- Точка подключения `/api/Fees/{id}`
- Метод `DELETE`
- Ожидаемый тип данных `application/json`


Пример ответа:

    {
        "id": 2,
        "percent": 1,
        "absolute": 8,
        "maxAmount": 1
    }


# Limits
## Get limits list

- Точка подключения `/api/Limits`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Пример ответа:

    {
        "TotalItems": 2,
        "Items": [
            {
                "Id": 1,
                "Name": "Tier 1",
                "DepositAch": 1000,
                "DepositCards": 2000,
                "Withdraw": 0,
                "Exchange": 0,
                "KycFieldsRequired": ["Address", "IdentityDoc"]

            },
            {
                "Id": 3,
                "Name": "Tier 2",
                "DepositAch": 5000,
                "DepositCards": 6000,
                "Withdraw": 0,
                "Exchange": 0,
                "KycFieldsRequired": ["Email", "Phone"]

            }
        ]
    }

## Add limit

- Точка подключения `/api/Limits`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
            "Title": 'Tier 2',
            "DepositACH": '33300',
            "DepositCards": '444000',
            "Withdraw": '555000',
            "Exchange": 1000,
            "KycFieldsRequired": ["Email", "Phone"]
    }

Где:

- **KycTier** - уровень пройденности KYC 
- **Value** - значение лимита

Пример ответа:

    {
        "Id": 4,
        "Name": "Tier 2",
        "DepositAch": 33300,
        "DepositCards": 444000,
        "Withdraw": 0,
        "Exchange": 1000,
        "KycFieldsRequired": ["Email", "Phone"]

    }

## Edit fee

- Точка подключения `/api/Limits/{id}`
- Метод `PUT`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
            "Id": 1,
            "Title": 'Tier 1',
            "DepositACH": '1000',
            "DepositCards": '2000',
            "Withdraw": '5000',
            "Exchange": 1000,
            "KycFieldsRequired": ["IdentityNumber", "ResidenceVerification"]
    }

*Id в URL'е и в модели должны совпадать*

В ответ приходит код `204` без тела

## Delete fee

- Точка подключения `/api/Limits/{id}`
- Метод `DELETE`
- Ожидаемый тип данных `application/json`


Пример ответа:

    {
        "Id": 1,
        "Name": "Tier 1",
        "DepositAch": 1000,
        "DepositCards": 2000,
        "Withdraw": 0,
        "Exchange": 1000,
        "KycFieldsRequired": ["IdentityNumber", "ResidenceVerification"]
    }


# Others
## Is Admin

- Точка подключения `/api/IsAdmin`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Возвращает код `200`, если пользователь из токена админ, и `401`, если не админ.

# Blocked users

## Block user

- Точка подключения `/api/BlockedUsers`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
        "UserId" : "55756"
    }

## Get blocked users' ids

- Точка подключения `/api/BlockedUsers`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Формат ответа:

    [
        {
            "Id": 1,
            "UserId": 556
        },
        {
            "Id": 2,
            "UserId": 5756
        },
        {
            "Id": 3,
            "UserId": 55756
        }
    ]

## Unblock user

- Точка подключения `/api/BlockedUsers/{id}`
- Метод `DELETE`
- Ожидаемый тип данных `application/json`

Формат ответа:

    {
        "Id": 1,
        "UserId": 1
    }


# Countries

## Add country

- Точка подключения `/api/Countries`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
        "Code" : "ru"
    }

## Get countries

- Точка подключения `/api/Countries`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Формат ответа:

    [
        {
            "Id": 7,
            "Code": "ua"
        },
        {
            "Id": 8,
            "Code": "ru"
        },
        {
            "Id": 9,
            "Code": "us"
        }
    ]

## Remove country

- Точка подключения `/api/Countries/{code}`
- Метод `DELETE`
- Ожидаемый тип данных `application/json`

Формат ответа: пусто с кодом `200`

# Admins

Вызовы должны в заголовке авторизации содержать токен суперадмина (пользователь с айди 3)

## Get admins' IDs

- Точка подключения `/api/Admins`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Формат ответа:

    {
        "Admins": [
            3,
            5,
            7,
            4
        ],
        "SuperAdmins": 3
    }
    
## Add admin

- Точка подключения `/api/Admins/{id}`
- Метод `POST`
- Ожидаемый тип данных `application/json`

В ответ придет значение id.

Если пользователь уже админ:

    {
        "error": "User has already been added"
    }

## Delete admin

- Точка подключения `/api/Admins/{id}`
- Метод `DELETE`
- Ожидаемый тип данных `application/json`

В ответ придет значение id.

# Withdraw

## Create withdraw order

- Точка подключения `/api/Withdraw/CreateOrder`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
	    "Amount":"12",
	    "FiatAccountId":"1",
	    "PIN" : "123123aA!",
	    "Destination":"swift"
    }
    
- Amount - сколько выводим
- FiatAccountId - идентификатор кошелька
- PIN - пин-код кошелька (хешируется фронтендом, поэтому созданные через веб кошельки вернут неверный пин)
- Destination - поле, где будет указываться, куда производить вывод (будет зависеть от реализации шлюза)

Успех: пустой ответ с кодом 200

Ошибки:

Если ПИН не верный:

    {
        "error": "Wrong password"
    }
    
Если на счету недостаточно средств:

    {
        "error": "Not enough funds"
    }
    
# Pay crypto

Платеж с криптокошелька пользователя. 

- Авторизация `Bearer token` пользователя 
- Точка подключения `/api/PayCrypto`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Ожидаемая модель:

    {
	    "Receiver":"0xf65Eb3eE81Fb78DF5d8F8a310DB435994dBC6430",
	    "Amount":"0.5",
	    "PIN" : "123123aA!",
	    "WalletId":"1"
    }

Если ПИН не верный:

    {
        "error": "Pin code is invalid"
    }
    
Если адрес получателя не валидный:

    {
        "error": "Receiver address is invalid"
    }

В случае успеха вернется хеш транзакции с кодом ответа 200. Например, для эфира: 0x37c07e4ad7e9fd5e00406a3e708958fb6dba8205310cfd4a4f24d94a9b25555e