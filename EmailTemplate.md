# Использование шаблонов транзакционных писем

*Для использования шаблонов необходим [SendGrid v3 Web API](https://sendgrid.com/docs/API_Reference/api_v3.html). Кастомизация шаблонов осуществляется с помощью шаблонизатора [Handlebars](https://sendgrid.com/docs/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates/).*
При отправке письма необходимо добавить JSON объект `"dynamic_template_data":{}` заполненный по примерам ниже в массив `"personalizations":[]`

---

## Параметры подключения

***Аккаунт buybitcoinach@gmail.com:***

**API Key:** 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

---

## Содержание

1. Шаблон универсального транзакционного письма (Multipurpose)
   1. [Общие параметры](#markdown-header-multipurpose-general)
   2. [Письмо с кнопкой / без кнопки](#markdown-header-multipurpose-letter-with-or-without-button)
   3. [Письмо с HTML контентом](#markdown-header-multipurpose-letter-with-html-content)
   4. [Пример полностью заполненного объекта данных](#markdown-header-multipurpose-full-test-json)
2. Шаблон чека заказа (Invoice)
   1. [Общие параметры](#markdown-header-invoice-general)
   2. [Информация о заказе](#markdown-header-invoice-order)
   3. [Пример полностью заполненного объекта данных](#markdown-header-invoice-full-test-json)
3. [Пример полного api запроса к SendGrid](#markdown-header-full-api-request-sendgrid)

---

### Multipurpose General

**Template ID:** d-29996aa9012f49aca364cbaa8b7ae814

#### Instance (*Обязательно*)

```json
"Instance": {
    "Title": "GetCrypto24",
    "Logo": "https://getcrypto24.com/img/icons/android-chrome-192x192.png"
},
```

В объекте *Instance* должен быть логотип приложения от имени которого отправляется письмо, а также название приложения для отображения в случае, если изображения запрещены или не могут отображаться в почтовом клиенте.

##### Subject (*Обязательно*)

```json
"Subject": "Welcome to GetCrypto24",
```

В объекте *Subject* должна быть указана тема письма.

##### Language

```json
"Language": "en",
```

Объект *Language* информирует почтовый клиент о языке письма

##### Email (*Обязательно*)

```json
"Email": {
    "Title": "Confirm your email",
    "Preview": "GetCrypto24 require email confirmation to allow exchnage",
```

Часть объекта *Email*, внутри него должна быть тема письма (используется также в заголовке внутри шаблона) и превью письма (краткое содержание письма, используется некоторыми почтовыми клиентами при отображении списка писем)

##### Footer

```json
"Footer": {
    "Active": true,
    "Unsubscribe": {
        "Active": true,
        "Text": "You received this email because you just signed up for a new account. If these emails get annoying, please feel free to",
        "Title": "unsubscribe",
        "Link": "https://getcrypto24.com/"
    },
    "Additional": {
        "Active": true,
        "Text": "2018 © GetCrypto24 - 1234 Main Street - Anywhere, MA - 56789"
    }
  }
```

В объекте *Footer* передаются параметры и тексты для футера письма.

Параметр *Active* регулирует отображение футера в целом: true - отображается, false - не отображается (при этом остальные поля можно не заполнять). Таким образом, **если отображение футера в письме не нужно**, то его можно скрыть передав `"Active": false`

Объект *Unsubscribe* должен содержать:

- Active: Регулирует отображение текста и ссылки на отписку
- Text: Текст перед ссылкой отписки
- Title: Заголовок ссылки отписки
- Link: Ссылка на отписку

Объект *Additional* должен содержать:

- Active: Регулирует отображение дополнительного текста
- Text: Любой дополнительный текст

---

### Multipurpose Letter with or without button

Для использования данной конфигурации шаблона **необходимо добавить объект *Template* со свойством `"Active": true` в объект *Email***

```json
"Email": {
    .....
    "Template": {
        "Active": true,
        "Text": "GetCrypto24 require email confirmation to allow exchnage. Please, click the button below to confirm your email.",
        "Button": {
            "Active": true,
            "Title": "Confirm email",
            "Link": "https://getcrypto24.com/",
            "BgColor": "#ffd600",
            "Color": "#555"
        }
    },
    .....
},
```

Объект *Template* должен содержать:

- Active: Регулирует режим шаблона. true - используется конфигурация шаблона "с кнопкой / без кнопки"
- Text: Текст письма, выводится после заголовка. Использование html контента запрещено.

Объект *Button* внутри объекта *Template* должен содержать:

- Active: Регулирует отображение кнопки. **Для использования шаблона "без кнопки" должен быть false, при этом остальные параметры в *Button* можно не передавать.**
- Title: Надпись на кнопке
- Link: Ссылка кнопки на целевое действие письма
- BgColor: Цвет фона кнопки. Должен соответствовать палетке сервиса от имени которого отправляется письмо.
- Color: Цвет надписи на кнопке. Должен быть контрастным по отношению к цвету фона.

---

### Multipurpose Letter with HTML content

Для использования данной конфигурации шаблона **необходимо добавить объект *HTML* со свойством `"Active": true` в объект *Email*. При этом объект *Template* обязательно должен быть со свойством `"Active": false` или отсутствовать**

```json
"Email": {
    .....
    "HTML": {
        "Active": true,
        "Content": "<table style='width:100%'> <tr> <th style='font-size:20px;'>Firstname</th> <th style='font-size:30px;'>Lastname</th> <th style='font-size:10px;'>Age</th> </tr> <tr> <td style='color: red;'>Jill</td> <td>Smith</td> <td>50</td> </tr> <tr> <td>Eve</td> <td style='color: red;'>Jackson</td> <td>94</td> </tr> <tr> <td>John</td> <td>Doe</td> <td style='color: red;'>80</td> </tr></table><br/><ul> <li>Coffee</li> <li>Tea</li> <li>Milk</li></ul><br/><div style='text-align:center;'><img src='https://www.w3schools.com/html/img_girl.jpg' alt='Italian Trulli'></div>"
    }
    .....
},
```

Объект *HTML* должен содержать:

- Active: Регулирует режим шаблона. true - используется конфигурация шаблона с html контентом. **При этом объект *Template* в объекте *Email* обязательно должен быть со свойством `"Active": false` или отсутствовать**.
- Content: Строка с HTML контентом, заполняет почти весь макет шаблона за исключением хэдера и футера. **Обязательно! двойные кавычки (") в передаваемой строке должны быть экранированы либо заменены на одинарные (')**

---

### Multipurpose Full test json

```json
{
    "Instance": {
        "Title": "GetCrypto24",
        "Logo": "https://getcrypto24.com/img/icons/android-chrome-192x192.png"
    },
    "Subject": "Welcome to GetCrypto24",
    "Language": "en",
    "Email": {
        "Title": "Confirm your email",
        "Preview": "GetCrypto24 require email confirmation to allow exchnage",
        "Template": {
            "Active": true,
            "Text": "GetCrypto24 require email confirmation to allow exchnage. Please, click the button below to confirm your email.",
            "Button": {
            "Active": true,
                "Title": "Confirm email",
                "Link": "https://getcrypto24.com/",
                "BgColor": "#ffd600",
                "Color": "#555"
            }
        },
        "HTML": {
            "Active": false,
            "Content": "<table style='width:100%'> <tr> <th style='font-size:20px;'>Firstname</th> <th style='font-size:30px;'>Lastname</th> <th style='font-size:10px;'>Age</th> </tr> <tr> <td style='color: red;'>Jill</td> <td>Smith</td> <td>50</td> </tr> <tr> <td>Eve</td> <td style='color: red;'>Jackson</td> <td>94</td> </tr> <tr> <td>John</td> <td>Doe</td> <td style='color: red;'>80</td> </tr></table><br/><ul> <li>Coffee</li> <li>Tea</li> <li>Milk</li></ul><br/><div style='text-align:center;'><img src='https://www.w3schools.com/html/img_girl.jpg' alt='Italian Trulli'></div>"
        }
    },
    "Footer": {
        "Active": true,
        "Unsubscribe": {
            "Active": true,
            "Text": "You received this email because you just signed up for a new account. If these emails get annoying, please feel free to",
            "Title": "unsubscribe",
            "Link": "https://getcrypto24.com/"
        },
        "Additional": {
            "Active": true,
            "Text": "2018 © GetCrypto24 - 1234 Main Street - Anywhere, MA - 56789"
        }
    }
}
```

---

### Invoice General

**ВСЕ данные ОБЯЗАТЕЛЬНЫ для этого шаблона!**

**Template ID:** d-ca31d72a50024008becb314fd1a900e0

Общие параметры письма

```json
"Subject": "You have a new invoice from SendGrid (INV04297586)",
"Preview": "GetCrypto24 require email confirmation to allow exchnage",
"Language": "en",
```

- **Subject** - Тема письма
- **Preview** - Краткое содержание письма, отображается в некоторых клиентах в списке писем
- **Language** - Язык письма

---

### Invoice Order

Параметры заказа

```json
"Email": {
    "MerchantName": "IKEA Store B.V. [NL]",
    "MerchantURL": "https://www.ikea.com/",
    "MerchantAddress": "Patissery Del Mar 915 Camino Del Mar Suite 100 Del Mar, CA 92014 858-788-987",
    "MerchantOrderId": "435085",
    "OrderId": "157",
    "PriceFiat": "34 901, 67",
    "CurrencyFiat": "$",
    "PriceCrypto": "0,159546784750790",
    "CurrencyCrypto": "ETH",
    "TxLink": "https://etherscan.io/tx/0xf16572d323be96604ef93dee576feac072a0c8825728ae090ff2f997ffad896d",
    "Date": "Feb 14 2019 at 12:01 PM",
    "UserEmail": "alekseytyumentsev@gmail.com"
}
```

Объект *Email* должен содержать:

- **MerchantName** - Название магазина от которого создан заказ
- **MerchantURL** - Ссылка на сайта магазина
- **MerchantAddress** - Физический или юридический адрес магазина
- **MerchantOrderId** - Номер заказа в магазине
- **OrderId** - Номер заказа в процессинге
- **PriceFiat** - Стоимость заказа в фиатной валюте
- **CurrencyFiat** - Символ фиатной валюты (отображается перед стоимостью)
- **PriceCrypto** - Стоимость заказа в криптовалюте
- **CurrencyCrypto** - Символ криптовалюты (отображается после стоимости)
- **TxLink** - Ссылка на транзакцию
- **Date** - Дата заказа
- **UserEmail** - Email покупателя

---

### Invoice full test json

```json
{
    "Subject": "You have a new invoice from SendGrid (INV157)",
    "Preview": "We're writing to let you know that your 06/06/2019 GotoCrypto invoice (INV157), for account number 435085",
    "Language": "en",
    "Email": {
        "MerchantName": "IKEA Store",
        "MerchantURL": "https://www.ikea.com/",
        "MerchantAddress": "Patissery Del Mar 915 Camino Del Mar Suite 100 Del Mar, CA 92014 858-788-987",
        "MerchantOrderId": "435085",
        "OrderId": "157",
        "PriceFiat": "34 901,67",
        "CurrencyFiat": "$",
        "PriceCrypto": "0,159546784750790",
        "CurrencyCrypto": "ETH",
        "TxLink": "https://etherscan.io/tx/0xf16572d323be96604ef93dee576feac072a0c8825728ae090ff2f997ffad896d",
        "Date": "Feb 14 2019 at 12:01 PM",
        "UserEmail": "alekseytyumentsev@gmail.com"
    }
}
```

### Full API Request Sendgrid

`curl -X "POST" "https://api.sendgrid.com/v3/mail/send" -H "Authorization: Bearer YOUR_API_KEY" -H "Content-Type: application/json" -d "[YOUR DATA HERE]"`

Для запроса к SendGrid на отправку письма с чеком необходимо выполнить `POST` запрос на адрес `https://api.sendgrid.com/v3/mail/send` с двумя обазятельным HTTP заголовками:

- "Authorization: Bearer YOUR_API_KEY"
- "Content-Type: application/json"

В теле запроса должен быть JSON объект такого вида:

```json
{
    "template_id": "d-ca31d72a50024008becb314fd1a900e0",
    "from": {
        "email": "noreply@go2crypto.express",
        "name": "GotoCrypto"
    },
    "personalizations": [
        {
            "to": [
                {
                    "email": "pparker@mailinator.com",
                    "name": "Piter Parker"
                }
            ],
            "dynamic_template_data": {
                "Subject": "You have a new invoice from GotoCrypto (INV157)",
                "Preview": "We're writing to let you know that your 06/06/2019 GotoCrypto invoice (INV157), for account number 435085",
                "Language": "en",
                "Email": {
                    "MerchantName": "IKEA Store B.V.",
                    "MerchantURL": "https://www.ikea.com/",
                    "MerchantAddress": "Patissery Del Mar 915 Camino Del Mar Suite 100 Del Mar, CA 92014 858-788-987",
                    "MerchantOrderId": "435085",
                    "OrderId": "157",
                    "PriceFiat": "34 901,67",
                    "CurrencyFiat": "$",
                    "PriceCrypto": "0,159546784750790",
                    "CurrencyCrypto": "ETH",
                    "TxLink": "https://etherscan.io/tx/0xf16572d323be96604ef93dee576feac072a0c8825728ae090ff2f997ffad896d",
                    "Date": "Feb 14 2019 at 12:01 PM",
                    "UserEmail": "alekseytyumentsev@gmail.com"
                }
            }
        }
    ]
}
```
