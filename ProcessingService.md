# Процессинг

## Get all orders

- Точка подключения `/api/Orders`
- Метод `GET`
- Ожидаемый тип данных `application/json`
- Параметры запроса

    - **Query** - текст запроса. Ищется в полях `Receiver`, `PaymentAddress`
    - **PageNumber** - определяет номер страницы. Для вывода первой страницы задать `0`
    - **ItemsPerPage** - размер страницы 
    - **OrderColumn** - поле для сортировки: `OrderId`, `UserId`, `Created`, `Amount`, `Receiver`, `PaymentAddress`, `WaitingBalance`, `ActualBalance`, `OrderStatus`, `OrderedCurrency`
    - **SortDirection** - направление сортировки: `asc`, `desc`
    - **StartDate**, **EndDate** - фильтр по диапазону дат
    - **UserId** - фильтр по идентификатору пользователя
    - **Currency** - фильтр по валюте заказа
    - **StartAmount**, **EndAmount** - фильтр по сумме заказа (верхняя и нижняя граница)


В случае ошибки вернется ответ с кодом `500` или `400` в формате

    {
        "error": "message"
    }

- В случае успеха вернется объект `Items` c массивом заказов:

    - **UserId** - идентификатор пользователя, создавшего заказ
    - **Created** - дата создания заказа
    - **Amount** - количество приобретаемой крипты
    - **Receiver** - адрес кошелька получателя
    - **PaymentAddress** - платежный адрес
    - **WaitingBalance** - цена заказа
    - **ActualBalance** - фактический баланс платежного адреса
    - **OrderStatus** - текущий статус заказа
    - **OrderedCurrency** - заказанная валюта

Пример:

    {
        "orderId": 2,
        "userId": "GUID",
        "created": "2018-11-23T20:22:17.7691312",
        "amount": 123,
        "receiver": "17i5rSkMzWDHjVxNPcsrDqReXMuHGhw4Si",
        "paymentAddress": "0xa404C516B029DC5bA508dB0bC12fF31d70B1BA5E",
        "waitingBalance": 321,
        "actualBalance": 0,
        "orderStatus": "Waiting",
        "orderedCurrency": "BTC"
    }


## Get currencies list

- Точка подключения `/api/CurrencyList`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Входных данных не требуется. В случае серверной ошибки вернется ответ с кодом `500` в формате

    {
        "error": "message"
    }

В случае успеха вернется:

    {
    "Items": [
        {
        "Name": "BTC",
        "Code": "Bitcoin"
        },
        {
        "Name": "ETH",
        "Code": "Ethereum"
        },
        {
        "Name": "USD",
        "Code": "United States Dollar"
        },
        {
        "Name": "EUR",
        "Code": "Euro"
        }
    ]
    }

# Buffer keys

## Get keys

- Точка подключения `/api/BufferWallets`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Входных данных не требуется, токен должен принадлежать админу

Пример выходных данных:

    [
        {
            "Address": null,
            "Currency": "BTC",
            "Balance": "0"
        },
        {
            "Address": "0x616F0F303BF877C8DbF8f38B65f988D74eB380e9",
            "Currency": "ETH",
            "Balance": "0,0001"
        }
    ]

Для установленных кошельков приходят их адреса. Для не заданных - `null`

## Update key

- Точка подключения `/api/BufferWallets`
- Метод `POST`
- Ожидаемый тип данных `application/json`


Входная модель:

    {
        "Currency" : "ETH",
        "PrivateKey" : "0x2fde1fbc1a0f8558c7299976657e5461a80934788356f1c025579560de19d5e9"
    }

Устанавливает приватный ключ для буферного кошелька заданной крипты. Ключ валидируется, в случае неудачи возвращается ошибка

    {
        "error": "Private key invalid"
    }

## Order's transactions

- Точка подключения `/api/Transaction/{id}`
- Метод `GET`
- Ожидаемый тип данных `application/json`

Авторизационный токен должен принадлежать админу, а в id передается идентификор существующего заказа

Пример выходных данных:

    {
        "TotalItems": 9,
        "Items": [
            {
                "Id": 1,
                "TxHash": null,
                "RawTx": null,
                "Result": "Signing error Invalid base 58 string",
                "AttemptDate": null,
                "Confirmations": 0
            },
            {
                "Id": 3,
                "TxHash": null,
                "RawTx": null,
                "Result": "Not enough funds",
                "AttemptDate": null,
                "Confirmations": 0
            },
            {
                "Id": 9,
                "TxHash": null,
                "RawTx": null,
                "Result": "Transaction not signed",
                "AttemptDate": "2018-12-12T17:06:32.530931",
                "Confirmations": 0
            },
            {
                "Id": 11,
                "TxHash": null,
                "RawTx": null,
                "Result": "Not enough funds",
                "AttemptDate": null,
                "Confirmations": 0
            },
            {
                "Id": 13,
                "TxHash": "c59d736b18219eb4a06a639ad9f6f668a7ce5ac7fd9ef4fa2f5fb6c07c3c76da",
                "RawTx": "010000000173a300fe1b8b778a52903105cf164cecf722e3679de785e99c405da82299c857010000006a473044022067f5ab9be307789aba3cf882011c88664af5b6c25e4b254d6272eff71453a96d02203333726cb157e8808626494c169b87d5ce59de5284a289ce75c870f77202d355012103ead6f90506bb94e57f1edd15cede25ac451b0623543a7203b5f5f2e4f059ed05ffffffff02e4650000000000001976a9144995dc1ac6704a73b4caae7d03d195007ac072d488ac7e4b0800000000001976a9147ef018b5a8d787ad1876d246ab9489ced87c4d0d88ac00000000",
                "Result": "Success",
                "AttemptDate": "2018-12-12T17:45:41.6663443",
                "Confirmations": 1
            }
        ]
    }
    
## Create order

- Точка подключения `/api/Order`
- Метод `POST`
- Ожидаемый тип данных `application/json`

Пример входных данных:

    {
        "merchant_id": "2938", // ОБЯЗАТЕЛЬНО Merchant ID in GotoCrypto
        "hash": "5EC6024791AF6C394A903E237578E78B1EDE34DD228F86386673DE8B34523DB72C42610DC11454AE510B14FBBF3142B3C5D74C5FB00865DE8794C487129E50E4",
        "type": "onetime", // ОБЯЗАТЕЛЬНО Тип платежа
        "currency_code": "USD", // ОБЯЗАТЕЛЬНО Валюта заказа
        "price": "579.56", // ОБЯЗАТЕЛЬНО Итоговая стоимость заказа
        "merchant_order_id": "952417836", // ОБЯЗАТЕЛЬНО Номер заказа у мерчанта
        "tax": "20.82", // Налог на весь заказ

        "success_url": "https://avengers.com/success",  // ОБЯЗАТЕЛЬНО Адрес для редиректа пользователя после успешной оплаты
        "cancel_url": "https://avengers.com/cancel", // ОБЯЗАТЕЛЬНО Адрес для редиректа пользователя в случае отмены или неудачной оплаты
        "custom": "[{'requestId: '2908734'},{ 'customToken': 'slkan29837'}]", // Bypass поле для передачи напрямую в "notify_url"

        "customer": { // Информация о покупателе
            "email": "spiderman@avengers.com", // ОБЯЗАТЕЛЬНО
            "first_name": "Peter",
            "last_name": "Parker",
            "country": "USA",
            "zip": "658045",
            "state": "NY",
            "city": "NY",
            "address1": "20 Ingram Street", // 2 строки адреса
            "address2": "" // 2 строки адреса
    },

    "items": [ // Массив товаров
                {
                    "name": "Web Shooter", // ОБЯЗАТЕЛЬНО Название товара
                    "price": "58.12", // ОБЯЗАТЕЛЬНО Стоимость товара
                    "tax": "2.34", // Налог на товар
                    "number": "893468", // Артикул товара
                    "amount": "3", // Количество товара
                    "description": "The Web-Shooters are a pair of wrist-mounted mechanical devices" // Описание товара
                }
            ]
    }
    
Поле `hash` вычисляется следующим образом: SHA512(merchant_id + pass_phrase), а затем приводится к верхнему регистру.
Например, если `merchant_id` == 1, а `pass_phrase` == "secret", то хеш будет:
`5EC6024791AF6C394A903E237578E78B1EDE34DD228F86386673DE8B34523DB72C42610DC11454AE510B14FBBF3142B3C5D74C5FB00865DE8794C487129E50E4`
    
Возможные ошибки:

- `merchant not found`
- `wrong hash`
- `order already exists` - если заказ с таким `merchant_order_id` уже зарегистрирован