# Аутентификация, авторизация и учет

**Почему заголовки английские?** Потому что bitbucket не умеет делать внутренние ссылки в документа на русские заголовки.

## Table of content

 1. Тестовый [сервер](#markdown-header-development-server)
 2. Выбор [языка](#markdown-header-select-messages-language) сообщений 
 3. [Регистрация](#markdown-header-regiser-user-account) пользователя 
 4. Вход зарегистрированного пользователя
 5. Запрос верификации номера телефона по СМС
 6. Подтверждение номера телефона кодом из СМС
 7. [Заполнение](#markdown-header-create-person) персональной информации
 8. [Получение](#markdown-header-get-person) персональной информации
 9. [Обновление](#markdown-header-update-person) персональной информации
 10. Список [пользователей](#markdown-header-user-list)
 11. Получить данные [учетной записи](#markdown-header-user-profile) пользователя
 12. Список [групп](#markdown-header-group-list)
 13. Список [пользователей группы](#markdown-header-group-user-list)
 14. [Создание](#markdown-header-create-user-address) адреса пользователя
 15. [Изменение](#markdown-header-update-user-address) адреса пользователя
 16. [Получение](#markdown-header-get-user-address) адреса пользователя
 17. [Создание](#markdown-header-create-user-identity-card) удостоверение личности пользователя
 18. [Изменение](#markdown-header-update-user-identity-card) удостоверение личности пользователя
 19. [Получение](#markdown-header-get-user-identity-card) удостоверение личности пользователя
 20. [Получение](#markdown-header-get-user-roles) списка разрешений пользователя
 21. [Получение](#markdown-header-get-user-person-by-user-id) персональной информации пользователя по идентификатору пользователя
 22. [Получение](#markdown-header-get-user-address-by-user-id) адреса пользователя по идентификатору пользователя
 23. [Получение](#markdown-header-get-user-identity-card-by-user-id) удостоверения личности пользователя по идентификатору пользователя
 24. [Выгрузка](#markdown-header-upload-identity-card) удостоверения личности
 25. [Получение](#markdown-header-get-identity-card) удостоверения личности
 26. [Обновление](#markdown-header-update-identity-card) удостоверения личности
 27. [Выгрузка](#markdown-header-upload-identity-card-back-side) оборотная сторона удостоверения личности
 28. [Получение](#markdown-header-get-identity-card-back-side) оборотная сторона удостоверения личности
 29. [Обновление](#markdown-header-update-identity-card-back-side) оборотная сторона удостоверения личности 
 30. [Выгрузка](#markdown-header-upload-selfie-with-identity-card) селфи с удостоверением личности
 31. [Получение](#markdown-header-get-selfie-with-identity-card) селфи с удостоверением личности
 32. [Обновление](#markdown-header-update-selfie-with-identity-card) селфи с удостоверением личности
 33. [Выгрузка](#markdown-header-upload-utility-bill) квитанции ЖКХ пользователя
 34. [Получение](#markdown-header-get-utility-bill) квитанции ЖКХ пользователя
 35. [Обновление](#markdown-header-update-utility-bill) квитанции ЖКХ пользователя
 36. [Выгрузка](#markdown-header-upload-kyc-video) KYC видео пользователя
 37. [Получение](#markdown-header-get-kyc-video) KYC видео пользователя
 38. [Обновление](#markdown-header-update-kyc-video) KYC видео пользователя
 39. [Список](#markdown-header-country-list) стран и двухсимвольных ISO кодов стран
 40. [Список](#markdown-header-us-states-list) наименований и кодов североамериканских штатов 
 41. [Выгрузка](#markdown-header-upload-investor-certificate) сертификата инвестора
 42. [Получение](#markdown-header-get-investor-certificate) сертификата инвестора
 43. [Обновление](#markdown-header-update-investor-certificate) сертификата инвестора
 44. [Получить](#markdown-header-kyc-meta-info) набор полей KYC моделей
 45. [Получить](#markdown-header-kyc-state-info) информацию о заполненности полей KYC и проверке
 46. [Создание](#markdown-header-create-user-ssn) social security number (SSN)
 47. [Изменение](#markdown-header-update-user-ssn) social security number (SSN)
 48. [Получение](#markdown-header-get-user-ssn) social security number (SSN)
 49. [Получение](#markdown-header-get-user-ssn-by-user-id) social security number (SSN) по идентификатору пользователя
 

## Development server

`Development` версия тестового сервера доступна по адресу `https://auth8.azurewebsites.net`. Для доступа к методам сервера можно использовать тестовые учетные записи с именами `Tony Stark` и `Jack Sparrow`. Пароль у всех учетных записей одинаковый `P@ssw0rd` (вместо буквы 'О' ноль). 

Учетная запись `Tony Stark` обладает правами на выполнение всех методов API. Остальные учетные записи обладают правами лишь на просмотр и изменение собственного профиля. Ограничение междоменных запросов на тестовом сервере отключено. 

Развертывание `Development` версии на тестовый сервер производится `CI` сервисом на базе Jenkins автоматически, после обновления ветки `master` в Git репозитории проекта.

[К оглавлению ^](#markdown-header-table-of-content)

## Select messages language

Для выбора языка сообщений ответов методов API, бэкенд берет первое значение стандартного заголовка `Accept-Language`:

        Accept-Language: en

        Accept-Language: de-CH

        Accept-Language: fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5

Если в бэкенде не найдется ресурсов для для указанного языка или локали, сервер вернет ответ на английском языке.

[К оглавлению ^](#markdown-header-table-of-content)

## Regiser user account
 
 - Точка подключения `/api/account/register`
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Email: 'jack@sparrow.com',
            Phone: '+7 912 922 92 92',
            Password: 'P@ssw0rd',
            PasswordConfirmation: 'P@ssw0rd'
        }

Если указанный пользователем `Email` или `Phone` уже были зарегистрированы ранее, сервер вернет ответ `400 Bad request` с соответствующим сообщением.

		Email jack@sparrow.com already registered!

		Phone number +7 912 922 92 92 already registered!


Автоматические тесты для метода можно почитать в файле `api-tests\tests\register_successful_tests.js`

[К оглавлению ^](#markdown-header-table-of-content)

## Вход зарегистрированного пользователя
 
 - Точка подключения `/api/account/login`
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Login: 'jack@sparrow.com',
            Password: 'P@ssw0rd',
        }

В качестве `Login` можно указывать имя, адрес электронной почты или телефон зарегистрированного пользователя.

В случае успешного входа, сервер возвращает JSON, содержащий в том числе `AccessToken`. Для аутентификации всех последующих запросов, значение `AccessToken` следует отправлять со всеми последующими запросами, в заголовке  `Authorization`:

        'Authorization': `Bearer ${AccessToken}`

Автоматические тесты для метода можно почитать в файле `api-tests\tests\login_successful_tests.js`

[К оглавлению ^](#markdown-header-table-of-content)

## Запрос верификации номера телефона по СМС
 
 - Точка подключения `/api/phone/requestverification`
 - Точка подключения требует авторизации. К запросу обязательно приложить заголовок `Authorization: Bearer ${AccessToken}`. `AccessToken` можно получить выполнив вход.
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Phone: '+1 336 361 20 06'
        }

В случае, если номер телефона в запросе, не соответствует номеру телефона, указанному пользователем при регистрации, сервер вернет ответ:

        { 
            error: 'Указанный номер телефона не соответствует номеру, указанному пользователем при регистрации!' 
        }

Для правильного запроса сервер вернет ответ:
 
        { 
            Message: 'СМС с кодом подтверждения отправлено на номер телефона +1 336 361 20 06.' 
        }

Автоматические тесты для метода можно почитать в файле `api-tests\tests\phone_requestverification_tests.js`

[К оглавлению ^](#markdown-header-table-of-content)

## Подтверждение номера телефона кодом из СМС
 
 - До обращения к этой точке подключения необходимо запросить отправку проверочного кода в СМС, обратившись к точке подключения `/api/phone/requestverification`.
 - Точка подключения `/api/phone/verify`
 - Точка подключения требует авторизации. К запросу обязательно приложить заголовок `Authorization: Bearer ${AccessToken}`. `AccessToken` можно получить выполнив вход. 
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Phone: '+1 336 361 20 06',
            Text: 'укажите здесь код из СМС'
        }

В случае отправки несоответствующего кода, сервер вернет ответ:

        { 
            error: 'Неверный код проверки номера телефона '05847'!' 
        }

В случае успешного завершения проверки, сервер вернет ответ:

        { 
            Message: 'Проверка номера телефона +1 336 361 20 06 выполнена успешно.' 
        }

Автоматические тесты для метода можно почитать в файле `api-tests\tests\phone_verify_fault_tests.js`

[К оглавлению ^](#markdown-header-table-of-content)

## Create person
 
 - Точка подключения `/api/person`
 - Точка подключения требует авторизации. К запросу обязательно приложить заголовок `Authorization: Bearer ${AccessToken}`. `AccessToken` можно получить выполнив вход.
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            FirstName: 'Walter',
            MiddleName: 'Bruce',
            LastName: 'Willis',
            Gender: true,
            BirthDate: '1955/03/19',
            BirthPlace: 'Idar-Oberstein, Rheinland-Pfalz, Bundesrepublik Deutschland'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get person
 
 - Точка подключения `/api/person`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов возвращает персональную информацию **авторизованного** пользователя, чей токен приложен к заголовку `Authorization`, если он ее ранее заполнял.

[К оглавлению ^](#markdown-header-table-of-content)

## Update person
 
 - Точка подключения `/api/person`
 - Точка подключения **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
			 Id: {идентификатор ранее созданной записи}
            FirstName: 'Walter',
            MiddleName: 'Bruce',
            LastName: 'Willis',
            Gender: true,
            BirthDate: '1955/03/19',
            BirthPlace: 'Idar-Oberstein, Rheinland-Pfalz, Bundesrepublik Deutschland'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## User list

Постраничный вывод списка учетных записей пользователей. Запрос **требует авторизации**. Запрашивающий пользователь должен иметь **разрешение** на выполнение операции `View user list`.

 - Точка подключения `/api/userlist` 
 - Метод `GET`
 - Ожидаемый тип данных `application/x-www-form-urlencoded`
 - Принимает параметры
     
    - **Query** - поисковая строка запроса в возвращаемых данных. Пример `tony frank 333`. Может содержать несколько слов, разделяемых пробелами. Бэкенд будет искать указанные слова в данных таблицы.
    - **ItemsPerPage** - целое число. Устанавливает количество содержащихся на возвращаемой странице записей.
    - **PageNumber** - целое число. Устанавливает номер выбираемой страницы.
    - **OrderColumn** - строка. Устанавливает наименование колонки, по которой необходимо выполнить сортировку данных.
    - **SortDirection** - строка. Устанавливает направление сортировки данных в колонке. Может принимать значения `Ascending` и `Descending`.
    - **PropertyName** - Наименование колонки, по которой необходимо выполнить дополнительный отбор данных. Например можно выбирать данные какого-нибудь конкретного пользователя, указав `PropertyName=UserRecordId`.
    - **PropertyValue** - Значение, которое будет выбирать бэкенд для указанного `PropertyName`. Если например мы хотим просмотреть записи только одного пользователя, то указываем `/api/userlist?PropertyName=UserRecordId&PropertyValue=1`. 

**Пример** запроса, на выбор второй страницы из 10 учетных записей, телефон которых содержит подстроку `+7 495`, упорядоченных по номеру телефона в порядке убывания:

    /api/userlist?Query=%2B7%20495&ItemsPerPage=10&PageNumber=1&OrderColumn=Phone&SortDirection=Descending

[К оглавлению ^](#markdown-header-table-of-content)

## User profile

Данные учетной записи пользователя.

 - Точка подключения `/api/profile`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов возвращает информацию об учетной записи **авторизованного** пользователя, чей токен приложен к заголовку `Authorization`.

Успешный запрос возвращает JSON:
    
    {
        "Id":3,
        "Name":"Tony Stark",
        "Email":"tony@stark.local",
        "Phone":"+1 541 754 3010",
        "Password":"Here is encrypted password really!",
        "DenyAccess":false,
        "IsPhoneVerified":true,
        "IsEmailConfirmed":true,
        "OwnerUserId":3,
        "OwnerGroupId":3,
        "EditorAddress":"127.0.0.1",
        "EditorUserId":3,
        "EditorGroupId":3,
        "DateTime":"2018-11-18T13:46:03.573"
    }

[К оглавлению ^](#markdown-header-table-of-content)

## Group list

Постраничный вывод списка групп пользователей. Запрос **требует авторизации**. Запрашивающий пользователь **должен иметь разрешение** на выполнение операции `View group list`.

 - Точка подключения `/api/grouplist`
 - Метод `GET`
 - Ожидаемый тип данных `application/x-www-form-urlencoded`
 - Принимает параметры
     
    - **Query** - поисковая строка запроса в возвращаемых данных.
    - **ItemsPerPage** - целое число. Устанавливает количество содержащихся на возвращаемой странице записей.
    - **PageNumber** - целое число. Устанавливает номер выбираемой страницы.
    - **OrderColumn** - строка. Устанавливает наименование колонки, по которой необходимо выполнить сортировку данных.
    - **SortDirection** - строка. Устанавливает направление сортировки данных в колонке. Может принимать значения `Ascending` и `Descending`.
    - **PropertyName** - Наименование колонки, по которой необходимо выполнить дополнительный отбор данных. Например можно выбирать данные какого-нибудь конкретного пользователя, указав `PropertyName=UserRecordId`.
    - **PropertyValue** - Значение, которое будет выбирать бэкенд для указанного `PropertyName`. Если например мы хотим просмотреть записи только одного пользователя, то указываем `/api/userlist?PropertyName=UserRecordId&PropertyValue=1`. 

Пример строки запроса можно посмотреть в описании запроса списка [учетных записей пользователей](#markdown-header-user-list).

[К оглавлению ^](#markdown-header-table-of-content)


## Group user list

Постраничный вывод списка групп пользователей. Запрос **требует авторизации**. Запрашивающий пользователь **должен иметь разрешение** на выполнение операции `View group list`.

 - Точка подключения `/api/grouplist`
 - Метод `GET`
 - Ожидаемый тип данных `application/x-www-form-urlencoded`
 - Принимает параметры
     
    - **Query** - поисковая строка запроса в возвращаемых данных.
    - **ItemsPerPage** - целое число. Устанавливает количество содержащихся на возвращаемой странице записей.
    - **PageNumber** - целое число. Устанавливает номер выбираемой страницы.
    - **OrderColumn** - строка. Устанавливает наименование колонки, по которой необходимо выполнить сортировку данных.
    - **SortDirection** - строка. Устанавливает направление сортировки данных в колонке. Может принимать значения `Ascending` и `Descending`.
    - **PropertyName** - Наименование колонки, по которой необходимо выполнить дополнительный отбор данных. Например можно выбирать данные какого-нибудь конкретного пользователя, указав `PropertyName=UserRecordId`.
    - **PropertyValue** - Значение, которое будет выбирать бэкенд для указанного `PropertyName`. Если например мы хотим просмотреть записи только одного пользователя, то указываем `/api/userlist?PropertyName=UserRecordId&PropertyValue=1`. 

Пример строки запроса можно посмотреть в описании запроса списка [учетных записей пользователей](#markdown-header-user-list).

[К оглавлению ^](#markdown-header-table-of-content)

## Create user address
 
 - Точка подключения `/api/useraddress` Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Name: 'Payer address',
            Country: 'United Kingdom',
            Region: 'Greater London',
            ZipCode: 'W1B 1NU',
            City: 'London',
            Street: 'Marylebone, 86 Portland Pl'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update user address
 
 - Точка подключения `/api/useraddress` Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Id: {идентификатор ранее созданной записи}
            Name: 'Payer address',
            Country: 'United Kingdom',
            Region: 'Greater London',
            ZipCode: 'W1B 1NU',
            City: 'London',
            Street: 'Marylebone, 86 Portland Pl'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get user address
 
 - Точка подключения `/api/useraddress`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов адрес **авторизованного** пользователя, чей токен приложен к заголовку `Authorization`, если он его ранее заполнял.

[К оглавлению ^](#markdown-header-table-of-content)

## Create user identity card
 
 - Точка подключения `/api/identitycard` Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Name: 'Passport',
            Country: 'United Kingdom',
            Number: '982B177376',
            Authority: 'Her Majesty's Passport Office',
            DateOfIssue: '2015-05-03',
            DateOfExpiry: '2025-05-03'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update user identity card
 
 - Точка подключения `/api/identitycard` Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Id: {идентификатор ранее созданной записи}
            Name: 'Passport',
            Country: 'United Kingdom',
            Number: '982B177376',
            Authority: 'Her Majesty's Passport Office',
            DateOfIssue: '2015-05-03',
            DateOfExpiry: '2025-05-03'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get user identity card
 
 - Точка подключения `/api/identitycard`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов возвращает удостоверение личности **авторизованного** пользователя, чей токен приложен к заголовку `Authorization`, если он его ранее заполнял.

[К оглавлению ^](#markdown-header-table-of-content)

## Get user roles
 
 - Точка подключения `/api/userrolelist`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов возвращает массив наименований разрешений **авторизованного** назначенных пользователю, чей токен приложен к заголовку `Authorization`.

[К оглавлению ^](#markdown-header-table-of-content)


## Get user person by user id
 
 - Точка подключения `/api/user/{id}/person`
 - Точка подключения **требует авторизации**. Запрашивающий пользователь **должен иметь разрешение** на выполнение операции `View the person`.
 - Метод `GET`
 - Вызов возвращает персональную информацию пользователя, чей id указан в URL запроса

[К оглавлению ^](#markdown-header-table-of-content)

## Get user address by user id
 
 - Точка подключения `/api/user/{id}/address`
 - Точка подключения **требует авторизации**. Запрашивающий пользователь **должен иметь разрешение** на выполнение операции `View user address`.
 - Метод `GET`
 - Вызов возвращает адрес пользователя, чей id указан в URL запроса

[К оглавлению ^](#markdown-header-table-of-content)

## Get user identity card by user id
 
 - Точка подключения `/api/user/{id}/identitycard`
 - Точка подключения **требует авторизации**. Запрашивающий пользователь **должен иметь разрешение** на выполнение операции `View user identity card`.
 - Метод `GET`
 - Вызов возвращает удостоверение личности пользователя, чей id указан в URL запроса

[К оглавлению ^](#markdown-header-table-of-content)

## Upload identity card
 
 - Точка подключения `/api/identitycard/image`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycard',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get identity card
 
 - Точка подключения `/api/identitycard/image`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал изображение удостоверения личности, сервер вернет объект, содержащий информацию о загруженном изображении.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycard',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update identity card
 
 - Точка подключения `/api/identitycard/image`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешного обновления, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycard',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Upload identity card back side
 
 - Точка подключения `/api/identitycard/imageback`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycardback',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get identity card back side
 
 - Точка подключения `/api/identitycard/imageback`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал изображение оборотной стороны удостоверения личности, сервер вернет объект, содержащий информацию о загруженном изображении.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycardback',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update identity card back side
 
 - Точка подключения `/api/identitycard/imageback`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешного обновления, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'identitycardback',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)


## Upload selfie with identity card
 
 - Точка подключения `/api/identitycard/selfie`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'selfie',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get selfie with identity card
 
 - Точка подключения `/api/identitycard/selfie`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал изображение селфи с удостоверением личности, сервер вернет объект, содержащий информацию о загруженном изображении.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'selfie',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update selfie with identity card
 
 - Точка подключения `/api/identitycard/selfie`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешного обновления, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'selfie',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Upload utility bill
 
 - Точка подключения `/api/useraddress/image`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'utilitybill',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get utility bill
 
 - Точка подключения `/api/useraddress/image`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал изображение квитанции ЖКХ, сервер вернет объект, содержащий информацию о загруженном изображении.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'utilitybill',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update utility bill
 
 - Точка подключения `/api/useraddress/image`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешного обновления, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'utilitybill',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Upload kyc video
 
 - Точка подключения `/api/uservideo`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `video/webm`
 - Кроме файла, сервер ожидает в загружаемых данных строковое поле `Screenplay` со строкой в формате `JSON`, содержащей массив с описанием набора действий, выполняемых пользователем на приложенном видео.

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'kycvideo',
            Screenplay: '[\'Turn left\',\'Smile\']',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.webm',
            ContentType: 'video/webm',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.webm',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get kyc video
 
 - Точка подключения `/api/uservideo`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал KYC видео, сервер вернет объект, содержащий информацию о загруженном видео.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'kycvideo',
            Screenplay: '[\'Turn left\',\'Smile\']',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.webm',
            ContentType: 'video/webm',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.webm',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update kyc video
 
 - Точка подключения `/api/uservideo`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `video/webm`
 - Кроме файла, сервер ожидает в загружаемых данных строковое поле `Screenplay` со строкой в формате `JSON`, содержащей массив с описанием набора действий, выполняемых пользователем на приложенном видео.

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'kycvideo',
            Screenplay: '[\'Turn left\',\'Smile\']',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.webm',
            ContentType: 'video/webm',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.webm',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Country list
 
 - Точка подключения `/api/country`
 - Метод `GET`

Для получения полного списка стран необходимо отправлять `GET` запрос `/api/country`. Ответ будет содержать массив объектов:

        [
			{ name: 'United Kingdom', code: 'uk' },
			{ name: 'Russia', code: 'ru' },
			{ name: 'United States', code: 'us' }
		]

Для получения элемента по коду, отправлять `GET` запрос `/api/country?code=uk`. Ответ будет содержать объект:

        { 
			name: 'United Kingdom', 
			code: 'uk' 
		}

Для получения элемента по наименованию, отправлять `GET` запрос `/api/country?name=Russia`. Ответ будет содержать объект:

        { 
			name: 'Russia', 
			code: 'ru' 
		}

Если элемент не найден ответ будет содержать `null`. Код ответа `200`.

[К оглавлению ^](#markdown-header-table-of-content)

## US states list
 
 - Точка подключения `/api/usstates`
 - Метод `GET`

Для получения полного списка штатов необходимо отправлять `GET` запрос `/api/usstate`. Ответ будет содержать массив объектов:

        [
			{ name: 'Alabama', code: 'AL' },
			{ name: 'Alaska', code: 'AK' },
			{ name: 'Wyoming', code: 'WY' }
		]

Для получения элемента по коду, отправлять `GET` запрос `/api/usstate?code=AL`. Ответ будет содержать объект:

        { 
			name: 'Alabama', 
			code: 'AL' 
		}

Для получения элемента по наименованию, отправлять `GET` запрос `/api/usstate?name=Alabama`. Ответ будет содержать объект:

        { 
			name: 'Alabama', 
			code: 'AL' 
		}

Если элемент не найден ответ будет содержать `null`. Код ответа `200`.

[К оглавлению ^](#markdown-header-table-of-content)

## Upload investor certificate
 
 - Точка подключения `/api/investorcertificate/image`
 - Запрос **требует авторизации**.
 - Метод `POST`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешной загрузки, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'investorcertificate',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Get investor certificate
 
 - Точка подключения `/api/investorcertificate/image`
 - Запрос **требует авторизации**.
 - Метод `GET`

В случае, если пользователь ранее загружал изображение сертификата инвестора, сервер вернет объект, содержащий информацию о загруженном изображении.

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'investorcertificate',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update investor certificate
 
 - Точка подключения `/api/investorcertificate/image`
 - Запрос **требует авторизации**.
 - Метод `PUT`
 - Ожидаемый тип данных `multipart/form-data`
 - Сервер принимает только изображения в формате `image/jpeg`

В случае успешного обновления, сервер возвращает объект:

        { 
            Id: 44,
            UserRecordId: 95,
            Name: 'investorcertificate',
            Container: '8862a829-fb7f-4048-878d-10f9c97ed44a',
            OriginalFileName: '576x768.jpg',
            ContentType: 'image/jpeg',
            ContentLength: 94331,
            Url: 'https://starticodemo.blob.core.windows.net/8862a829-fb7f-4048-878d-10f9c97ed44a/9da7d212-66e6-44cc-a84c-8af5e085893b.jpg',
        }

[К оглавлению ^](#markdown-header-table-of-content)

## KYC meta info
 
 - Точка подключения `/api/kyc/meta`
 - Запрос **требует авторизации** и наличия у запрашивающего разрешения `View KYC meta`
 - Метод `GET`

Cервер возвращает массив объектов:

		[ 
			{ type: 'image', field: 'UtilityBill', description: 'Utility bill' },
			{ type: 'video', field: 'KycVideo', description: 'video selfie' },
			{ type: 'image', field: 'IdentityCard', description: 'Identity card' },
			{ type: 'image', field: 'InvestorCertificate', description: 'Investor certificate' },
			{ type: 'Person', field: 'FirstName', description: 'First name' },
			{ type: 'Person', field: 'MiddleName', description: 'Middle name' },
			{ type: 'Person', field: 'LastName', description: 'Last name' },
			{ type: 'Person', field: 'Gender', description: 'Gender' },
			{ type: 'Person', field: 'BirthDate', description: 'Birth date' },
			{ type: 'Person', field: 'BirthPlace', description: 'Birth place' },
			{ type: 'Address', field: 'Country', description: 'Country' },
			{ type: 'Address', field: 'Region', description: 'Region' },
			{ type: 'Address', field: 'ZipCode', description: 'Zip code' },
			{ type: 'Address', field: 'City', description: 'City' },
			{ type: 'Address', field: 'Street', description: 'Street' },
			{ type: 'IdentityCard', field: 'Number', description: 'Number' },
			{ type: 'IdentityCard', field: 'Country', description: 'Country' },
			{ type: 'IdentityCard', field: 'Authority', description: 'Authority' },
			{ type: 'IdentityCard', field: 'DateOfIssue', description: 'Date of issue' },
			{ type: 'IdentityCard', field: 'DateOfExpiry', description: 'Date of expiry' } 
		]
[К оглавлению ^](#markdown-header-table-of-content)

## KYC state info
 
 - Точка подключения `/api/kyc/{userId}/state`
 - Запрос **требует авторизации** и наличия у запрашивающего разрешения `View KYC state`
 - Метод `GET`

Cервер возвращает массив объектов:

        [ 
            { type: 'image', field: 'IdentityCard', value: 'http://localhost/Tony-Stark/identitycard.jpg' },
            { type: 'image', field: 'UtilityBill', value: 'http://localhost/Tony-Stark/utilitybill.jpg' },
            { type: 'image', field: 'InvestorCertificate', value: 'http://localhost/Tony-Stark/investorcertificate.jpg' },
            { type: 'video', field: 'KycVideo', value: 'http://localhost/Tony-Stark/kycvideo.webm' },
            { type: 'User', field: 'Email', value: 'tony@stark.local' },
            { type: 'User', field: 'Phone', value: '+1 541 754 3010' },
            { type: 'Person', field: 'FirstName', value: 'Anthony' },
            { type: 'Person', field: 'MiddleName', value: 'Edward' },
            { type: 'Person', field: 'LastName', value: 'Stark' },
            { type: 'Person', field: 'Gender', value: true },
            { type: 'Person', field: 'BirthDate', value: '1963-05-03T00:00:00' },
            { type: 'Person', field: 'BirthPlace', value: 'New York' },
            { type: 'Address', field: 'Country', value: 'United States' },
            { type: 'Address', field: 'Region', value: 'California' },
            { type: 'Address', field: 'ZipCode', value: '90024' },
            { type: 'Address', field: 'City', value: 'Los Angeles' },
            { type: 'Address', field: 'Street', value: '3701, Los Amigos St.' },
            { type: 'IdentityCard', field: 'Country', value: 'United States' },
            { type: 'IdentityCard', field: 'Number', value: 'TONYSTARK' },
            { type: 'IdentityCard', field: 'Authority', value: 'United States Department of State' },
            { type: 'IdentityCard', field: 'DateOfIssue', value: '2018-05-05T00:00:00' },
            { type: 'IdentityCard', field: 'DateOfExpiry', value: '2028-05-05T00:00:00' },
            { type: 'State', field: 'State', value: 'Pending' } 
        ]

[К оглавлению ^](#markdown-header-table-of-content)

## Create user SSN
 
 - Точка подключения `/api/ssn` Запрос **требует авторизации**. 
 - Создает запись SSN для пользователя, от имени которого выполняется запрос (чей токен приложен к заголовку `Authorization`)
 - Метод `POST`
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Number: '175-29-1928'
        }

[К оглавлению ^](#markdown-header-table-of-content)

## Update user SSN
 
 - Точка подключения `/api/ssn` Запрос **требует авторизации**. 
 - Метод `PUT`
 - Обновляет запись SSN для пользователя, от имени которого выполняется запрос (чей токен приложен к заголовку `Authorization`)
 - Ожидаемый тип данных `application/json`
 - Модель должна содержать:
    
        {
            Number: '738-10-2816'
        }

Если пользователь ранее не заполнял свой номер SSN, запрос вернет `null`.

[К оглавлению ^](#markdown-header-table-of-content)

## Get user SSN
 
 - Точка подключения `/api/ssn`
 - Точка подключения **требует авторизации**.
 - Метод `GET`
 - Вызов возвращает SSN **авторизованного** пользователя, чей токен приложен к заголовку `Authorization`, если он его ранее заполнял или `null`, если пользователь не заполнял номер SSN.
 - Если пользователь ранее не заполнял свой номер SSN, запрос вернет `null`.

[К оглавлению ^](#markdown-header-table-of-content)

## Get user SSN by user id
 
 - Точка подключения `/api/ssn/{userId}`
 - Точка подключения **требует авторизации** и наличия у запрашивающего разрешения `View the person`.
 - Метод `GET`
 - Если пользователь, чей `userId` указан в `URL` запроса, ранее не заполнял свой номер SSN, запрос вернет `null`.

 В случае успеха запрос вернет:

	{
		Id: идентификатор_записи,
		UserRecordId: идентификатор_пользователя,
		Number: '382-01-3492'
	}

[К оглавлению ^](#markdown-header-table-of-content)
