# Имена, пароли, адреса, ресурсы, группы

## Swagger
getcrypto24 api swagger online documentation
 
Developer test stand - http://getcrypto24test.com:8000/api

common url - <host>/api if Swagger engine enabled for this enveroment

## Группа PaymentSystem

    {
        'databaseServer': { 
            'name': 'paymentsystemdbserver.database.windows.net',
            'users': [
                {
                    'login': 'psdbowner',
                    'isAdmin': 'true',
                    'password': 'Nh1An87!lnz:SnAy9)PzWQbT9-Rr$#mkx',
                    'database': 'master'
                },
                {
                    'login': 'authservice',
                    'isAdmin': 'false',
                    'password': 'k!nGA z@ndU B00M',
                    'database': 'auth_db'
                }                   
            ],
        }
        'applicationServer: [
            {
                'name': 'Authentication, authorization, accounting and KYC API service',
                'developmentUrl': 'https://auth8.azurewebsites.net'
            },
            {
                'name': 'KYC frontend',
                'developmentUrl': 'https://kyc8.azurewebsites.net/'
            }             
        ]
    }

## Группа cryptoexchange

    {
        'databaseServer': { 
            'name': 'cryptoexchangedbserver.database.windows.net',
            'users': [
                {
                    'login': 'dbuser',
                    'isAdmin': 'true',
                    'password': 'm0Nga tonG@ p!ngU',
                    'database': 'master'
                }                 
            ],
        }
        'applicationServer: [
            {
                'name': 'Cryptoexchange application frontend',
                'developmentUrl': 'https://getcrypto24.azurewebsites.net'
            },
            {
                'name': 'Cryptoexchange admin interface',
                'developmentUrl': 'https://admin-getcrypto24.azurewebsites.net'
            },
            {
                'name': 'Cryptoexchange backend',
                'developmentUrl': 'https://getcrypto24-backend.azurewebsites.net'
            }             
        ]
    }
