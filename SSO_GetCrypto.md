# Authorization in the application GetCrypto24 using a Bearer token

To authorize the GetCrypto24 application using the Bearer token, you must send a GET request to the GetCrypto24 SSO page with the authorization token in the "AuthToken" parameter.
***The address from which the request is made (HTTP Referer) must be in the list of allowed addresses for authorization in the GetCrypto24 application. Now there are two addresses: https://bitcoinalley.com/ and https://www.bitcoinalley.com/.***

#### Parameter list:

- AuthToken (*Required*) - Authorization Token

#### Sample request for authorization
`
https://getcrypto24.com/sso?AuthToken=Bearer%20eyJhbGciJHRwOi8vd3dzLm9yZyA0L3htbGRzaWcW9yZSN...
`

Upon successful login, the user will go to the Dashboard page. In case of an error, the user will return to the site from which the request was made (HTTP Referer) or to the Login page of the GetCrypto24 application, if the address of the site could not be determined.

***To test this feature, you can use the address https://getcrypto24test.com/***