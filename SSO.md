# Документация по использованию технологии единого входа в приложении Auth

## Содержание

1. [Список параметров](#markdown-header-url-parameters)
2. [Авторизация](#markdown-header-login)
3. [Регистрация](#markdown-header-register)
4. [Верификация номера телефона](#markdown-header-phone-number-verification)
5. [Сброс и установка пароля](#markdown-header-reset-password)
6. [Выход из приложения](#markdown-header-logout)
7. [Получение токена при обратном редиректе](#markdown-header-getting-a-token-after-external-sso-request)

*Адрес с которого производится запрос (HTTP Referer), а также адрес в параметре `Redirect` **должен быть в списке разрешённых адресов** для входящих запросов в приложении Auth. Приложение используещее Auth для авторизации/регистрации и др. **должно быть предварительно добавлено в базу данных приложений** и получить уникальный идентификатор `AppId`*

---

## URL Parameters

- `AppId` (***Обязательно***) - Идентификатор внешнего приложения в базе данных Auth
- `Mode` (***Обязательно***) - Режим запроса (авторизация/регистрация/...)
- `Redirect` - Адрес для обратного редиректа
- `Lang` - Код языковой локали
- `Return` - Возможность отмены обратного редиректа (только для режима деавторизации)

---

## Login

Для авторизации внешнего приложения необходимо выполнить редирект на страницу SSO c параметром `Mode=login`, а также идентификатором приложения в параметре `AppId`.

Дополнительно может быть передан адрес обратного редиректа в параметре `Redirect`. В этом параметре должен быть, если он передаётся, полный адрес до метода обрабатывающего токен из входного параметра адресной строки. Если этот параметр не передан, то будут использованы данные из справочника приложений. Также можно задать локаль приложения с помощью параметра `Lang`, он должен содержать пятисимвольный код локали, например `Lang=es-ES`.

После успешной авторизации, пользователь будет автоматически перенаправлен (далее в тексте: обратный редирект) с токеном в параметре `AuthToken` и временем истечения токена в параметре `ExpirationDate` на адрес указанный в параметре `Redirect` или из справочника приложений.

### Пример запроса авторизации

`https://login.gotocrypto.cash?AppId=7&Mode=login&Redirect=https://kyc.getcrypto24.com/login&Lang=it-IT`

---

## Register

Запрос аналогичен за исключением параметра `Mode=register`.
После успешной регистрации, пользователь будет авторизован и перейдёт на страницу подтверждения номера телефона. В случае успешного подтверждения произойдёт обратный редирект.
  
### Пример запроса регистрации

`https://login.gotocrypto.cash?AppId=5Mode=register&Redirect=https://account.gotocrypto.cash/sso&Lang=fr-FR`

---

## Phone number verification

Запрос аналогичен за исключением параметра `Mode=verify`.
После успешной верификации номера телефона произойдёт обратный редирект.

### Пример запроса верификации номера телефона

`https://login.gotocrypto.cash?AppId=3&Mode=verify&Redirect=https://admin.getcrypto24.com/sso&Lang=es-ES`

---

## Reset password

Запрос аналогичен за исключением параметра `Mode=resetpwd`.
После успешной установки нового пароля, пользователь перейдёт на страницу входа. После авторизации произойдёт обратный редирект.

### Пример запроса на сброс и установку пароля

`https://login.gotocrypto.cash?AppId=2&Mode=resetpwd`

---

## Logout

Запрос аналогичен за исключением параметра `Mode=logout`. Если в запросе передан параметр `Return=false`, после деавторизации пользователь не будет перенаправлен в исходное приложение, а перейдёт на страницу входа. Иначе после деавторизации произойдёт обратный редирект.

### Пример запроса на выход

`https://login.gotocrypto.cash?AppId=6&Mode=logout`

---

## Getting a token after external SSO request

После успешной авторизации или, в случае, если пользователь уже авторизован, произойдёт обратный редирект на адрес указанный в параметре `Redirect` или из справочника приложений с авторизационным токеном в параметре `AuthToken` и временем истечения токена в параметре `ExpirationDate`.

### Пример обратного редиректа

`https://kyc.getcrypto24.com/login?AuthToken=Bearer%20eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0IiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdC9hcGkiLCJpYXQiOjE1NDU4MjI1MzksImV4cCI6MTU0NTg1ODUzOSwic3ViIjoiVG9ueSBTdGFyayIsInVzZXJfaWQiOjMsImdyb3VwcyI6WzEsMywtMV0sInJvbGVzIjpbIkFkZCB1c2VyIHRvIGdyb3VwIiwiVmlldyB1c2VyIGdyb3VwIiwiUmVtb3ZlIHVzZXIgZnJvbSBncm91cCIsIkNyZWF0ZSB0aGUgZ3JvdXAiLCJVcGRhdGUgdGhlIGdyb3VwIiwiVmlldyB0aGUgZ3JvdXAiLCJEZWxldGUgdGhlIGdyb3VwIiwiVmlldyBncm91cCBsaXN0IiwiVmlldyBsaXN0IG9mIHVzZXIgZ3JvdXBzIiwiVmlldyB0aGUgcm9sZSIsIlZpZXcgcm9sZXMiLCJWaWV3IHVzZXIgcm9sZXMiLCJDcmVhdGUgdGltZSB6b25lIiwiVXBkYXRlIHRpbWUgem9uZSIsIlZpZXcgdGltZSB6b25lIiwiRGVsZXRlIHRpbWUgem9uZSIsIlZpZXcgdGltZSB6b25lIGxpc3QiLCJWaWV3IHVzZXIgdGltZSB6b25lIGxpc3QiLCJDcmVhdGUgdGhlIHVzZXIiLCJVcGRhdGUgdGhlIHVzZXIiLCJWaWV3IHRoZSB1c2VyIiwiRGVsZXRlIHRoZSB1c2VyIiwiVmlldyB1c2VyIGxpc3QiLCJBZGQgdXNlciB0byByb2xlIiwiVmlldyB1c2VyIHJvbGUiLCJEZWxldGUgdXNlciBmcm9tIHJvbGUiLCJBc3NpZ24gdGltZSB6b25lIHRvIHVzZXIiLCJWaWV3IHVzZXIgdGltZSB6b25lIiwiQ3JlYXRlIHRoZSBwZXJzb24iLCJVcGRhdGUgdGhlIHBlcnNvbiIsIlZpZXcgdGhlIHBlcnNvbiIsIkRlbGV0ZSB0aGUgcGVyc29uIiwiQ3JlYXRlIHVzZXIgYWRkcmVzcyIsIlVwZGF0ZSB1c2VyIGFkZHJlc3MiLCJWaWV3IHVzZXIgYWRkcmVzcyIsIkRlbGV0ZSB1c2VyIGFkZHJlc3MiLCJDcmVhdGUgdXNlciBpZGVudGl0eSBjYXJkIiwiVXBkYXRlIHVzZXIgaWRlbnRpdHkgY2FyZCIsIlZpZXcgdXNlciBpZGVudGl0eSBjYXJkIiwiRGVsZXRlIHVzZXIgaWRlbnRpdHkgY2FyZCIsIkV4Y2hhbmdlQWRtaW4iLCJQcm9jZXNzaW5nQWRtaW4iLCJWaWV3IEtZQyBtZXRhIiwiVmlldyBLWUMgc3RhdGUiXX0.uI6jAcCFVEgU_n9DMHxXL5M28RYlmYrU777kM6EBFyU&ExpirationDate=2019-07-02T03%3A42%3A51`
