# GotoCrypto Checkout Application

GotoCrypto Checkout gives your buyers a simplified and secure checkout experience with cryptocurrencies. The person who clicks the payment button is redirected from the merchant site to the GotoCrypto Checkout and then back to the merchant site.

## Pay button

People who pay you through GotoCrypto Checkout interact with HTML forms and hidden HTML input variables that you place on your website. When someone clicks a payment button in an HTML form on a webpage, the form submits the variables and their values to GotoCrypto Checkout. You set the values of the variables to produce the desired effect, for example, the order number, cost, buyer email and more.

### Form

To get started you need to create an HTML form from scratch with desired variables. The `form` tag includes two required attributes, `action` and `method`, which should look like this:

```html
<form action="https://go2crypto.express" method="get">
    // Variables
</form>
```

Do not change these values. These attributes are required for payment button.

### Allowed variables

- **Id** (*Required*) - Merchant ID in GotoCrypto
- **OrderId** (*Required*) - Order number in merchant's shop
- **Price** (*Required*) - Order value in US dollars
- **Email** - Buyer Email

The variable marked as "(*Required*)" means that in case of absence or incorrect filling the buyer will not be able to pay for the order and see the error message, and then back to the merchant site.

Optional variables such as email may be missing. To pay for the order, the buyer will need to fill in the email field in the application. Anyway, the buyer can always change the email field in the application.

Value of each variable should be filled in `value` field with name of valiable filled in `name`. `type` field should be hidden. For example:

```html
    <input type="hidden" name="Id" value="1">
```

### Example button code

The following sample HTML code shows a payment button with variables for automatically filling out GotoCrypto Checkout forms for the buyer. Your website generates the field entries dynamically from information that your website gathers about the buyer. The variables and their values are included in the URL to which buyers are sent when they click the payment button.

```html
<form action="https://go2crypto.express" method="get">
    <input type="hidden" name="Id" value="1">
    <input type="hidden" name="OrderId" value="00534">
    <input type="hidden" name="Price" value="54,25">
    <button type="submit">Pay with GotoCrypto</button>
</form>
```

## Pay process

1. Checks for the presence of all required parameters.
2. Merchant ID is sent to the GotoCrypto server to verify and obtain information about the seller.
3. The order is temporarily stored in the local storage of the buyer's browser.
4. The buyer can fill/edit the email field, agrees to the terms of use of the application, and creates an order in the GotoCrypto server by pressing the payment button with one of the cryptocurrencies.
5. The buyer is shown the amount to pay for the order in the selected cryptocurrency and the address where the buyer needs to send money. Additionally, the QR code with the address for payment is displayed.
6. Next, GotoCrypto Checkout application waits for the transfer of the required amount of money to the issued crypto-currency address by monitoring the blockchain.
7. As soon as the GotoCrypto server records the receipt of funds, the buyer will be shown a notice of successful payment. This is only a preliminary notice to the buyer. Full confirmation usually takes several tens of minutes.
8. After a 10 second timeout, the buyer will return to the merchant site using the link filled in the GotoCrypto server.
9. Further, the merchant site should expect to be informed about the full payment of the order from the GotoCrypto server to the specified URL filled in the GotoCrypto server. More on this process will be written later...
